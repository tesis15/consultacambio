/** DECLARACION DE MODULOS QUE VAMOS A EMPLEAR PARA ESTE CASO USAREMOS MARIADB PARA CONSUMIR LA BASE
* Y EXPRESS COMO SERVIDOR PARA EXPONER EL SERVICIO 
*/

const express = require("express");
const mariadb = require('mariadb');
const app=express();

/**SE DECLARAN DOS VARIABLES QUE ES EL PUERTO POR EL CUAL CORRERA NUESTRO SERVICO Y QUE ACEPTARA LAS PETICIONES
 * DESDE CUALQUIER IP
 */


var port=process.env.SERVERPORT || 8081
var serverip=process.env.SERVERIP || '0.0.0.0'

/**SE DECLARAN DOS VARIABLES QUE ES EL ENCARGADO DE CONEXION A LA BASE DE DATOS
 */
const pool = mariadb.createPool({
    host: process.env.DATABASE_URL || '192.168.0.27',
    user: process.env.DATABASE_USER || 'tesis',
    password: process.env.DATABASE_PASSWORD || 'serviciotesis',
    database: process.env.DATABASE_NAME || 'SERVICIOS',
    connectionLimit: 5
});


/** DECLARAMOS EL SERVICIO QUE VAMOS A EXPONER QUE ES UN GET Y LA URI DEL SERVICIO ASI COMO QUE VA A LEER EL VALOR DE LA 
 * DEL TIPO DE MONEDA A CONSULTAR Y FECHA DE NUESTRA BASE DE DATOS 
*/
app.get('/consulta/tipocambio', async function(req, res) {


    var valor = req.query.moneda;
    var fecha = req.query.fecha;


 try {
    conn = await pool.getConnection();
        row = await conn.query(`SELECT DATE_FORMAT(FECHA, '%d %M %Y') as FECHA, MONEDA, TIPO_CAMBIO FROM TIPO_CAMBIO WHERE MONEDA='${valor}'and FECHA='${fecha}'`);
    
    res.json(row);
    conn.release();
} catch (err) {
    conn.release();
    res.json({ status: false });
    throw err;
};
});




/** EXPONEMOS EL SERVIDOR DE EXPRESS  */

app.listen(port, serverip, function(req, res) {
   console.log("Servicios de tipo de cambio se encuentra arriba");
});



